// ==UserScript==
// @name         Vastausmaatti Edita AI
// @namespace    https://jaahas.codeberg.page/skriptit/
// @version      0.2
// @description  Tekee Editan tehtäviä. Vielä alpha-vaiheessa.
// @author       Jermu
// @match        https://digioppikirja.edita.fi/*
// @icon         https://upload.wikimedia.org/wikipedia/commons/d/d9/Flag_of_Norway.svg
// @grant        GM.getValue
// @grant        GM.setValue
// @grant        GM.xmlhttpRequest
// @require      https://cdn.jsdelivr.net/npm/@violentmonkey/shortcut@1
// @downloadURL  https://codeberg.org/jaahas/skriptit/raw/branch/main/vastausmaatti-edita-ai.user.js
// ==/UserScript==


VM.shortcut.register('c-.', async () => {
  try {
    malli = await GM.getValue("malli", "anthropic/claude-3.5-haiku")
    systemPrompt = await GM.getValue("systemPrompt", "Vastaa käyttäjän antamaan tehtävään, äläkä sano mitään muuta.")
    apiKey = await GM.getValue("apiKey", null)
    if (apiKey == null) {
      console.log("Arvoa apiKey ei ole asetettu. Aseta se skriptin \"Arvot\"-välilehdellä. Avaimen saat osoitteesta https://openrouter.ai/");
      document.getElementById(fElId).value = "Arvoa apiKey ei ole asetettu. Aseta se skriptin \"Arvot\"-välilehdellä. Avaimen saat osoitteesta https://openrouter.ai/";
      return
    }
    var focusedElement = document.activeElement;
    focusedElement.setAttribute('id', 'firstPracPara');
    var additionalInfo = focusedElement.textContent;
    var jaahas = document.getElementsByClassName("question-assignment")[0].firstElementChild.firstElementChild.children[1].textContent;
    console.log(jaahas)
    var ELE = focusedElement.parentElement.parentElement.previousElementSibling.firstElementChild.firstElementChild.lastElementChild;
    console.log(ELE);
    if (jaahas == null || jaahas == "") {
      var instruction = focusedElement.parentElement.parentElement.parentElement.parentElement.parentElement.previousElementSibling;
      var jaahas = instruction.parentElement.parentElement.parentElement.firstElementChild.children[1].firstElementChild.firstElementChild.firstElementChild.firstElementChild.textContent;
      var ELE = instruction.firstElementChild.firstElementChild.lastElementChild;
      console.log(jaahas)
      console.log(ELE);
    }
    var result = ELE.textContent;
    console.log(result);
    const request = await GM.xmlhttpRequest({
      method: "POST",
      url: "https://openrouter.ai/api/v1/chat/completions",
      headers: {
        "Authorization": "Bearer " + apiKey,
        "Content-Type": "application/json"
      },
      responseType: "json",
      data: JSON.stringify({
        "model": malli,
        "messages": [
          {"role": "system", "content": systemPrompt + "\n\n" + additionalInfo},
          {"role": "user", "content": "Task: " + jaahas + "\n\n" + result}
        ]
      }),
      onload: function(response) {
        const data = JSON.parse(response.responseText);
        insult = (data.choices[0].message.content);
        console.log(insult);
        document.getElementById('firstPracPara').firstElementChild.textContent = insult;
        document.getElementById('firstPracPara').removeAttribute("id");
      },
      onerror: function(error) {
        console.error("Error:", error);
      }
    });

  } catch(error) {
    console.log(error);
  }
});