// ==UserScript==
// @name        Vastausmaatti keuda.fi
// @namespace   https://jaahas.codeberg.page/skriptit/
// @match       https://pinja.keuda.fi/mod/quiz/attempt.php*
// @grant       none
// @version     0.1.1
// @author      Jermu
// @grant       GM.getValue
// @grant       GM.xmlhttpRequest
// @require     https://cdn.jsdelivr.net/npm/@violentmonkey/shortcut@1
// @description keudan tehtävät automatico
// @icon        https://upload.wikimedia.org/wikipedia/commons/f/f2/Flag_of_Great_Britain_%281707%E2%80%931800%29.svg
// @downloadURL https://codeberg.org/jaahas/skriptit/raw/branch/main/keuda.user.js
// ==/UserScript==



VM.shortcut.register('c-.', async () => {
  try {
    malli = await GM.getValue("malli", "anthropic/claude-3.5-sonnet")
    apiKey = await GM.getValue("apiKey", null)
    var focusedElement = document.activeElement;
    var fElId = focusedElement.id;
    var instruction = focusedElement.parentElement.previousElementSibling.previousElementSibling.firstElementChild.lastElementChild;
    var ohjeet = document.getElementById(instruction.id).textContent;
    let result = "";
    if (apiKey == null) {
      console.log("Arvoa apiKey ei ole asetettu. Aseta se skriptin \"Arvot\"-välilehdellä. Avaimen saat osoitteesta https://openrouter.ai/");
      return
    }
    console.log(ohjeet);
    if (focusedElement.textContent != null) {
      var additionalInfo = focusedElement.textContent
    } else {
      var additionalInfo = "Ei lisäohjeita."
    }
    console.log(additionalInfo);
    const request = await GM.xmlhttpRequest({
      method: "POST",
      url: "https://openrouter.ai/api/v1/chat/completions",
      headers: {
        "Authorization": "Bearer " + apiKey,
        "Content-Type": "application/json"
      },        
      responseType: "json",
      data: JSON.stringify({
        "model": malli,
        "messages": [
          {"role": "user", "content": "Solve step by step: " + ohjeet}
        ]
      }),
      onload: async function(response) {
        const data = JSON.parse(response.responseText);
        insult = (data.choices[0].message.content);
        console.log(insult);
        const request = await GM.xmlhttpRequest({
          method: "POST",
          url: "https://openrouter.ai/api/v1/chat/completions",
          headers: {
            "Authorization": "Bearer " + apiKey,
            "Content-Type": "application/json"
          },
          responseType: "json",
          data: JSON.stringify({
            "model": malli,
            "messages": [
              {"role": "user", "content": "Solve step by step: " + ohjeet},
              {"role": "assistant", "content": insult},
              {"role": "user", "content": "Now output only the solution, in plain text, like x=-11/2."}
            ]
          }),
          onload: function(response) {
            const data = JSON.parse(response.responseText);
            insult = (data.choices[0].message.content);
            if (insult.startsWith("x=")) {
                insult = insult.slice(2);
            };
            if (insult.startsWith("x = ")) {
                insult = insult.slice(4);
            };
            console.log("Lopullinen vastaus: " + insult);
            document.getElementById(fElId).value = insult;
          },
          onerror: function(error) {
            console.error("Error:", error);
          }
        });
      }
    })
  } catch(error) {
    console.log(error);
  }
});