// ==UserScript==
// @name         Vastausmaatti Otava
// @namespace    https://jaahas.codeberg.page/skriptit/
// @version      0.1
// @description  Tekee Otavan digikirjoista tietynlaiset tehtävät. Vielä alpha-vaiheessa.
// @author       Jermu
// @match        https://materiaalit.otava.fi/*
// @icon         https://upload.wikimedia.org/wikipedia/en/1/12/Flag_of_Poland.svg
// @grant        GM.getValue
// @grant        GM.setValue
// @require      https://cdn.jsdelivr.net/npm/@violentmonkey/shortcut@1
// @downloadURL  https://codeberg.org/jaahas/skriptit/raw/branch/main/vastausmaatti-otava.user.js
// ==/UserScript==




(async () => {

  setTimeout(async function() {

    if (await GM.getValue("ran", false)) {

      let awnwas =  JSON.parse(await GM.getValue("vastaus"));

      const ruudut = document.getElementsByClassName("fill-in");

      for (let i = 0; i < ruudut.length; i++) {
        ruudut[i].firstElementChild.value = awnwas[i];
      }

      await GM.setValue("ran", false)

    }
  }, 3000)
})();

VM.shortcut.register('c-.', () => {

  const vastausnappi = document.getElementById("check-answers-action");

  vastausnappi.classList.remove("btn-disabled");
  vastausnappi.ariaDisabled = "false";
  vastausnappi.click();

  setTimeout(async function() {
    const vastausnappi = document.getElementById("check-answers-action");

    vastausnappi.classList.remove("btn-disabled");
    vastausnappi.ariaDisabled = "false";
    vastausnappi.click();

    setTimeout(async function() {
      const vastausnappi = document.getElementById("check-answers-action");

      vastausnappi.classList.remove("btn-disabled");
      vastausnappi.ariaDisabled = "false";
      vastausnappi.click();

      const ruudut = document.getElementsByClassName("fill-in");

      if (ruudut.length == 0) {

        document.getElementById("toggle-answers-action").click();

      } else {

        document.getElementById("toggle-answers-action").click();

        const vastaukset = document.getElementsByClassName("correct-answer");

        const jaahas = [];

        for (let i = 0; i < vastaukset.length; i++) {
          jaahas.push(vastaukset[i].firstElementChild.value);
        }

        await GM.setValue("vastaus", JSON.stringify(jaahas));

        await GM.setValue("ran", true)

        document.getElementById("restart-task-action").click();

      }

    }, 100)

  }, 100)

});