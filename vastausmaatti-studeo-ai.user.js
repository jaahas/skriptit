// ==UserScript==
// @name         Vastausmaatti Studeo AI
// @namespace    https://jaahas.codeberg.page/skriptit/
// @version      0.2.1
// @description  Tekee Studeon tehtäviä. Vielä alpha-vaiheessa.
// @author       Jermu
// @match        https://app.studeo.fi/*
// @icon         https://upload.wikimedia.org/wikipedia/commons/d/d4/Flag_of_Israel.svg
// @grant        GM.getValue
// @grant        GM.xmlhttpRequest
// @require      https://cdn.jsdelivr.net/npm/@violentmonkey/shortcut@1
// @downloadURL  https://codeberg.org/jaahas/skriptit/raw/branch/main/vastausmaatti-studeo-ai.user.js
// ==/UserScript==

VM.shortcut.register('c-.', async () => {
  try {
    malli = await GM.getValue("malli", "anthropic/claude-3-haiku")
    systemPrompt = await GM.getValue("systemPrompt", "Vastaa käyttäjän antamaan tehtävään, äläkä sano mitään muuta.")
    apiKey = await GM.getValue("apiKey", null)
    var focusedElement = document.activeElement;
    var fElId = focusedElement.id;
    var instruction = focusedElement.parentElement.parentElement.parentElement.parentElement.previousElementSibling;
    var ohjeet = document.getElementById(instruction.id).children;
    let result = "";
    if (apiKey == null) {
      console.log("Arvoa apiKey ei ole asetettu. Aseta se skriptin \"Arvot\"-välilehdellä. Avaimen saat osoitteesta https://openrouter.ai/");
      document.getElementById(fElId).value = "Arvoa apiKey ei ole asetettu. Aseta se skriptin \"Arvot\"-välilehdellä. Avaimen saat osoitteesta https://openrouter.ai/";
      return
    }
    for (let i = 0; i < ohjeet.length; i++) {
      console.log(`${i}: ${ohjeet[i]}`);
      if (ohjeet[i] instanceof HTMLOListElement) {
        result += "\n";
        const theInstructions = ohjeet[i].children;
        for (let j = 0; j < theInstructions.length; j++) {
          result += `\n${String.fromCharCode(97 + j)}) ${theInstructions[j].textContent}`;
        }
        result += "\n";
      } else if (ohjeet[i] instanceof HTMLUListElement) {
        result += "\n";
        const theInstructions = ohjeet[i].children;
        for (let j = 0; j < theInstructions.length; j++) {
          result += `\n${theInstructions[j].textContent}`;
        }
        result += "\n";
      } else {
        result += `\n\n${ohjeet[i].textContent}`;
      }
    }
    result = result.trim();
    console.log(result);
    if (focusedElement.textContent != null) {
      var additionalInfo = focusedElement.textContent
    } else {
      var additionalInfo = "Ei lisäohjeita."
    }
    console.log(additionalInfo);
    const request = await GM.xmlhttpRequest({
      method: "POST",
      url: "https://openrouter.ai/api/v1/chat/completions",
      headers: {
        "Authorization": "Bearer " + apiKey,
        "Content-Type": "application/json"
      },
      responseType: "json",
      data: JSON.stringify({
        "model": malli,
        "messages": [
          {"role": "system", "content": systemPrompt},
          {"role": "user", "content": "Tehtävä: " + result + "\n\nLisäohjeet: " + additionalInfo}
        ]
      }),
      onload: function(response) {
        const data = JSON.parse(response.responseText);
        insult = (data.choices[0].message.content);
        console.log(insult);
        document.getElementById(fElId).value = insult;
      },
      onerror: function(error) {
        console.error("Error:", error);
      }
    });

  } catch(error) {
    console.log(error);
  }
});