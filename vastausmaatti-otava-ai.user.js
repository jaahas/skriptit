// ==UserScript==
// @name         Vastausmaatti Otava AI
// @namespace    https://jaahas.codeberg.page/skriptit/
// @version      0.1
// @description  Tekee Otavan jonkinlaisia tehtäviä. Vielä alpha-vaiheessa.
// @author       Jermu
// @match        https://materiaalit.otava.fi/*
// @icon         https://upload.wikimedia.org/wikipedia/commons/f/f6/Flag_of_Iraq.svg
// @grant        GM.getValue
// @grant        GM.setValue
// @grant        GM.xmlhttpRequest
// @require      https://cdn.jsdelivr.net/npm/@violentmonkey/shortcut@1
// @downloadURL  https://codeberg.org/jaahas/skriptit/raw/branch/main/vastausmaatti-otava-ai.user.js
// ==/UserScript==


VM.shortcut.register('c-.', async () => {
  try {
    malli = await GM.getValue("malli", "anthropic/claude-3-haiku")
    systemPrompt = await GM.getValue("systemPrompt", "Vastaa käyttäjän antamaan tehtävään, äläkä sano mitään muuta.")
    apiKey = await GM.getValue("apiKey", null)
    if (apiKey == null) {
      console.log("Arvoa apiKey ei ole asetettu. Aseta se skriptin \"Arvot\"-välilehdellä. Avaimen saat osoitteesta https://openrouter.ai/");
      document.getElementById(fElId).textContent = "Arvoa apiKey ei ole asetettu. Aseta se skriptin \"Arvot\"-välilehdellä. Avaimen saat osoitteesta https://openrouter.ai/";
      return
    }
    var focusedElement = document.activeElement;
    focusedElement.setAttribute('id', 'firstPracPara');
    var instruction = focusedElement.parentElement.parentElement.parentElement.parentElement.parentElement.previousElementSibling;
    console.log(instruction);
    systemPrompt2 = instruction.parentElement.parentElement.parentElement.firstElementChild.children[1].firstElementChild.firstElementChild.firstElementChild.firstElementChild.textContent;
    console.log(systemPrompt2)
    var ELE = instruction.firstElementChild.firstElementChild.lastElementChild;
    console.log(ELE);
    var result = ELE.textContent;
    console.log(result);
    const request = await GM.xmlhttpRequest({
      method: "POST",
      url: "https://openrouter.ai/api/v1/chat/completions",
      headers: {
        "Authorization": "Bearer " + apiKey,
        "Content-Type": "application/json"
      },
      responseType: "json",
      data: JSON.stringify({
        "model": malli,
        "messages": [
          {"role": "system", "content": systemPrompt},
          {"role": "user", "content": "Task: " + systemPrompt2},
          {"role": "user", "content": "Tehtävä: " + result}
        ]
      }),
      onload: function(response) {
        const data = JSON.parse(response.responseText);
        insult = (data.choices[0].message.content);
        console.log(insult);
        document.getElementById('firstPracPara').value = insult;
        document.getElementById('firstPracPara').removeAttribute("id");
      },
      onerror: function(error) {
        console.error("Error:", error);
      }
    });

  } catch(error) {
    console.log(error);
  }
});

function getTextContentOfChildren(element) {
  let textContent = '';
  for (let i = 0; i < element.children.length; i++) {
    textContent += element.children[i].textContent;
    if (i < element.children.length - 1) {
      textContent += '\n\n';
    }
  }
  return textContent;
}