// ==UserScript==
// @name         Abitti-vastauksien paljastaja
// @namespace    https://jaahas.codeberg.page/skriptit/
// @version      1.1
// @description  Paljastaa piilotetut vastaukset Abitissa
// @author       Jermu
// @match        https://oma.abitti.fi/answers/*
// @icon         https://upload.wikimedia.org/wikipedia/commons/a/a9/Flag_of_the_Soviet_Union.svg
// @downloadURL  https://codeberg.org/jaahas/skriptit/raw/branch/main/abitti-vastaukset.user.js
// @grant        none
// ==/UserScript==

var styles = `
  /* Info opiskelijalle */
  #answers section:before { content:'(Koekysymykset ei ole enää piilotettu :D)' !important; }
  /* Yleistä */
  #answers .exam-question-instruction { display: block !important; }
  /* Pudotusvalikot */
  #answers .exam-question > span:not(.text-answer, .e-inline-block, .e-dropdown-answer__answered) { visibility: visible !important; }
  #answers .e-dropdown-answer__correct { display: block !important; }
  /* Monivalinnat */
  #answers .exam-question > .exam-question > .exam-question-title::target-text { visibility: visible !important; }
  #answers .e-choice-answer-option:not(.e-choice-answer-option--selected) { display: block !important; }
  /* Alitehtävien tehtävänannot */
  #answers h4.exam-question-title { visibility: visible !important; }
  /* Leipätekstit */
  #answers .bn-hideable { visibility: visible !important; }
`;


var styleSheet = document.createElement("style");
styleSheet.innerText = styles;
document.head.appendChild(styleSheet);